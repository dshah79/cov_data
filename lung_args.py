import io
import random
from flask import Flask, Response, request
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.backends.backend_svg import FigureCanvasSVG
import matplotlib.pyplot as plt
import cv2
import tensorflow as tfs
from matplotlib.figure import Figure
from keras.models import *
from keras.layers import *
from keras.optimizers import *
from keras.losses import binary_crossentropy
from keras.utils import Sequence
from keras import backend as keras
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import load_model
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
import imageio
from glob import glob
import numpy as np
import os
from skimage import io
import argparse
from flask import Flask, render_template, send_from_directory, redirect, request
import os
import requests
from PIL import Image
import cv2
from pydicom import dcmread
from pydicom.filebase import DicomBytesIO
from PIL import Image
import urllib.request
from skimage.color import rgb2gray
from skimage import *
import os
import requests
SEG_MOD = os.path.join("./","segarch_unet_valpha.hdf5")
global im_or
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

#Processing image to train in the model, returning image array ready for analysis
def training_image(im_data):
    img_ary = im_data / 255
    img_ary = np.reshape(img_ary, img_ary.shape + (1,))
    print(type(im_data))
    img_ary = np.reshape(img_ary,(1,) + img_ary.shape)
    return img_ary

#Loading image array from the model , returning image data, casted in unsigned 8-bit format
def loading_image(img_ary):
    im_data = (img_ary[0,:, :, 0] * 255.).astype(np.uint8)
    return im_data

#Calculating dice_coefficients for the model loaded
def dice_coef(y_true, y_pred):
        y_true_f = keras.flatten(y_true)
        y_pred_f = keras.flatten(y_pred)
        intersection = keras.sum(y_true_f * y_pred_f)
        return (2. * intersection + 1) / (keras.sum(y_true_f) + keras.sum(y_pred_f) + 1)

#Computing dice coefficient loss for inputs to the model
def dice_coef_loss(y_true, y_pred):
        return -dice_coef(y_true, y_pred)

#Loading segmentation model following UNet Architecture
segmentation_model = load_model(SEG_MOD, custom_objects={'dice_coef_loss': dice_coef_loss, 'dice_coef': dice_coef})


#Segmentation of the image using the local model
def segmenting_img(img_name, img, save_to):
    global im_seg
    global im_or
    im_or = img
    print(img.shape)
    img = cv2.resize(img, (512, 512))
    print(img.shape)
    #with graph.as_default():
    segm_ret = segmentation_model.predict(training_image(img[:,:,0]), verbose=0)
    img = cv2.bitwise_and(img, img, mask=loading_image(segm_ret))
    im_seg = loading_image(segm_ret)
    #print("Max_Val")
    #print(list(map(max,im_seg)))
    im_seg = cv2.resize(im_seg,(im_or.shape[1],im_or.shape[0]))
    cv2.imwrite(os.path.join(save_to, "api_seg_"+"%s.png" % img_name), im_seg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--img_dir", type=str, required=True)
    parser.add_argument("--cov_dir", type = str, default= None)
    parser.add_argument("--visualize_dir", type=str, default=None)
    args = parser.parse_args()


    im_val = cv2.imread(args.img_dir)
    #im_val = rgb2gray(im_val_in)
    #im_val = cv2.cvtColor(im_val, cv2.COLOR_RGB2GRAY)
    print(im_val.dtype)
    print(im_val.shape)
    print(args.img_dir)
    res_path = os.path.basename(args.img_dir +"")
    print(res_path.split('.')[0])
    res_name = res_path.split('.')[0]

    print(res_name)
    target = os.path.join(APP_ROOT,"")
    destination = "/".join([target,res_name])
    if os.path.isfile(destination):
        os.remove(destination)
    segmenting_img(res_name,im_val,target)
